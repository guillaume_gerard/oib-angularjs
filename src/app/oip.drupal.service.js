(function() {
  'use strict';

  angular
    .module('oipApp')
    .factory('drupal', drupal);

  drupal.$inject = ['$window'];

  function drupal ($window) {
    return $window.Drupal || {};
  }
})();
