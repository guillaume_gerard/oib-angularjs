(function() {
  'use strict';

  /**
  * @ngdoc     controller
  * @name      oipApp.controller:thankYouController
  * @description
  *   The Thank You Controller.
  **/
  angular
  .module('oipApp')
  .controller('SorryController', SorryController);

  SorryController.$inject = ['$scope','$routeParams'];

  function SorryController ($scope, $routeParams) {
    var sorry = this;
    sorry.oipOrderId = $routeParams.orderId;
    sorry.oipOrderCreatePaymentUrl = 'http://192.168.33.10:9999/#/';
  }

})();
