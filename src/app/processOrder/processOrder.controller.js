(function() {
  'use strict';

  /**
  * @ngdoc     controller
  * @name      oipApp.controller:processOrderController
  * @description
  *   The Process Order Controller.
  **/
  angular
  .module('oipApp')
  .controller('ProcessOrderController', ProcessOrderController);

  ProcessOrderController.$inject = ['$routeParams', '$window', '$timeout', 'rpssOrder', 'oipOrder', 'drupal'];

  function ProcessOrderController ($routeParams, $window, $timeout, rpssOrder, oipOrder, drupal) {
    var
      processPage = this,
      data;

    processPage.requestError = false;
    processPage.requestSuccess = false;

    processPage.oipOrderId = $routeParams.orderId;
    processPage.remoteOrderId = oipOrder.getRemoteOrderId(processPage.oipOrderId);
    if (!processPage.remoteOrderId) {
      paymentError();
    }
    else {
      // TODO : Try to refactorize a bit.
      rpssOrder.getOrder(processPage.remoteOrderId, drupal.settings.oip_app_id)
        .then(function(result) {
          status = result.status;
          // TODO : Put valid / invalid state in order.services.js and use a getter
          if (status === 'error' || result.state !== 'Fulfilment') {
            paymentError();
          }
          else {
            data = {
              'correlationId': rpssOrder.getRandomCorrelationId(),
              'pssAppId': drupal.settings.oip_app_id
            };

            rpssOrder.settleOrder(processPage.remoteOrderId, data)
              .then(function(result) {
                if (result.status === 'OK') {
                  paymentSuccessful();
                }
                else {
                  paymentError();
                }
              })
              .catch(function() {
                paymentError();
              });
        }
      })
      .catch(function() {
        paymentError();
      });
    }

    function paymentSuccessful() {
      processPage.requestSuccess = true;
      $timeout(function () {
        $window.location.href = '#/thank-you/'+processPage.oipOrderId;
      }, 3000);
    }

    function paymentError() {
      processPage.requestError = true;
      $timeout(function () {
        $window.location.href = '#/sorry/'+processPage.oipOrderId;
      }, 1000);
    }
  }


})();
