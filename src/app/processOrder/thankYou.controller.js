(function() {
  'use strict';

  /**
  * @ngdoc     controller
  * @name      oipApp.controller:thankYouController
  * @description
  *   The Thank You Controller.
  **/
  angular
  .module('oipApp')
  .controller('ThankYouController', ThankYouController);

  ThankYouController.$inject = ['$scope','$routeParams'];

  function ThankYouController ($scope, $routeParams) {
    var thankYou = this;
    thankYou.oipOrderId = $routeParams.orderId;
    thankYou.oipOrderCreatePaymentUrl = 'http://192.168.33.10:9999/#/';
  }

})();
