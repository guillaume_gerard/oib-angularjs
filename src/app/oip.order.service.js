(function() {
  'use strict';

  angular
  .module('oipApp')
  .factory('oipOrder', oipOrder);

  function oipOrder() {
    var localStorageOrderName = 'oipOrders',
      order;

    order = {
      getLocalStorageOrderName: getLocalStorageOrderName,
      setLocalStorageOrderName: setLocalStorageOrderName,
      removeLocalStorageOrderName: removeLocalStorageOrderName,
      createOrderId: createOrderId,
      updateOrderId: updateOrderId,
      getNumberOfOrder : getNumberOfOrder,
      getRemoteOrderId : getRemoteOrderId
    };

    return order;

    function getLocalStorageOrderName() {
      return localStorageOrderName;
    }

    function setLocalStorageOrderName(localStorageName) {
      localStorageOrderName = localStorageName;
    }

    function removeLocalStorageOrderName(localStorageName) {
      localStorage.removeItem(localStorageName);
    }

    function createOrderId() {
      var
        newOipOrderId,
        oipOrdersParsed;

      newOipOrderId  = String(getNumberOfOrder() + 1);
      // First Item of the list.
      if (newOipOrderId === '1') {
        oipOrdersParsed = [ { oipOrder: newOipOrderId, remoteOrder: false	} ];
      }
      else {
        oipOrdersParsed = JSON.parse(localStorage.getItem(localStorageOrderName));
        oipOrdersParsed.push({
          oipOrder: newOipOrderId,
          remoteOrder: false
        });
      }
      localStorage.setItem(localStorageOrderName, JSON.stringify(oipOrdersParsed));
      // Return the new Oip Order Id created.
      return newOipOrderId;
    }

    function updateOrderId(oipOrderId, remoteOrderId) {
      var
        oipOrdersParsed = JSON.parse(localStorage.getItem(localStorageOrderName)),
        orderUpdated = false;
      angular.forEach(oipOrdersParsed, function(value) {
        if (value.oipOrder === oipOrderId) {
          value.remoteOrder = remoteOrderId;
          localStorage.setItem(localStorageOrderName, JSON.stringify(oipOrdersParsed));
          // TODO Understand why if I return true here the function doesnt stop.
          orderUpdated = true;
        }
      });

      return orderUpdated;
    }

    function getNumberOfOrder() {
      var oipOrdersParsed;
      if (localStorage.getItem(localStorageOrderName) === null) {
        return 0;
      }
      oipOrdersParsed = JSON.parse(localStorage.getItem(localStorageOrderName));
      return oipOrdersParsed.length;
    }

    function getRemoteOrderId(oipOrderId) {
      var
        oipOrdersParsed = JSON.parse(localStorage.getItem(localStorageOrderName)),
        remoteOrderId = false;
      angular.forEach(oipOrdersParsed, function(value) {
        if (value.oipOrder === oipOrderId) {
          remoteOrderId = value.remoteOrder;
        }
      });
      return remoteOrderId;
    }

  }

})();
