(function() {
  'use strict';

  /**
  * @ngdoc     controller
  * @name      oipApp.controller:PaymentFormController
  * @description
  *   The Payment Form Controller.
  **/
  angular
    .module('oipApp')
    .controller('PaymentFormController', PaymentFormController);

  PaymentFormController.$inject = ['$window', 'rpssOrder', 'oipOrder'];

  function PaymentFormController ($window, rpssOrder, oipOrder) {
    var
      paymentForm = this;

    paymentForm.accountName = 'My amazing account';
    paymentForm.vatAmount = 20;
    paymentForm.requestError = false;
    // Regex to test greater than 0 numbers.
    paymentForm.onlyNumbers = /^[1-9]+[0-9]*$/;
    paymentForm.oipOrderId =  oipOrder.createOrderId();

    paymentForm.updateGrossAmount = function() {
      var grossAmountToRound;
      grossAmountToRound = parseFloat(paymentForm.netAmount) + parseFloat(paymentForm.vatAmount);
      if (!isNaN(grossAmountToRound)) {
        // We need to safely round the result to avoid unexpected result.
        paymentForm.grossAmount = parseFloat(Math.round(grossAmountToRound * 100)/100);
      }
      // Reset the grossAmount field if we can't calculate the value.
      else {
        paymentForm.grossAmount = null;
      }
    };

    paymentForm.submitValidPaymentForm = function() {
      var
        lineItems,
        billingAddress,
        promise;

      lineItems = [
        {
          'appLineItemId':paymentForm.oipOrderId,
          'grossUnitPrice':parseFloat(paymentForm.grossAmount),
          'netUnitPrice':parseFloat(paymentForm.netAmount),
          'vatUnitAmount':parseFloat(paymentForm.vatAmount),
          'quantity':1
        }
      ];

      billingAddress = {
        'fullName':'I am OIP',
        'address1':'95 Wandsworth Rd',
        'address2':'null',
        'postTown':'LONDON',
        'county':'LONDON',
        'postcode':'SW82HG',
        'country':'GB'
      };

      promise = rpssOrder.createOrder(
        String(Math.floor(Math.random()*10000)),
        '18',
        'payg',
        '0',
        'rmltestuser5@example.com',
        1,
        'http://192.168.33.10:9999/#/',
        'http://192.168.33.10:9999/#/process-order/'+paymentForm.oipOrderId,
        paymentForm.oipOrderId,
        'oip',
        parseFloat(paymentForm.grossAmount),
        parseFloat(paymentForm.netAmount),
        parseFloat(paymentForm.vatAmount),
        1,
        lineItems,
        billingAddress);

      if (promise) {
        promise.then(function(result) {
          paymentForm.requestError = false;
          oipOrder.updateOrderId(paymentForm.oipOrderId, result.orderId);
          $window.location.href = result.pssResponseUrl;
        }).catch(function() {
          paymentForm.requestError = true;
        });
      }
      else {
        paymentForm.requestError = true;
      }

    };

    paymentForm.submitInvalidPaymentForm = function() {

      var promise;

      // We intentionally call createOrder without argument to return an error
      // and then display the expected error message on the page.
      promise = rpssOrder.createOrder();
      if (!promise) {
        paymentForm.requestError = true;
      }
    };

  }

})();
