// TODO : Create an Angular module for this file.
(function() {
  'use strict';

  angular
  .module('oipApp')
  .factory('rpssOrder',rpssOrder);

  rpssOrder.$inject = ['$http', 'drupal'];

  function rpssOrder($http, drupal) {
    var order;

    order = {
      getOrder: getOrder,
      prepareCreateOrder: prepareCreateOrder,
      createOrder: createOrder,
      settleOrder: settleOrder,
      getRandomCorrelationId : getRandomCorrelationId
    };

    return order;

    function getOrder(orderId, pssAppId) {
      var
        correlationId = getRandomCorrelationId(),
        get_order_url = drupal.settings.rpss_drupal_order_service_base_url+'/'+orderId+'?pssAppId='+pssAppId+'&correlationId='+correlationId,
        orderPromise;

      orderPromise = $http({
        method: 'GET',
        url: get_order_url,
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(function successCallback(response) {
        return response.data;
      }, function errorCallback(response) {
        return response.data;
      });
      return orderPromise;
    }

    function prepareCreateOrder(
      correlationId,
      userId,
      type,
      guid,
      email,
      productId,
      updateOrderUrl,
      checkoutCompleteUrl,
      appOrderId,
      pssAppId,
      grossTotalPrice,
      netTotalPrice,
      vatTotalAmount,
      totalQuantity,
      lineItems,
      billingAddress) {

      var order;

      order = {
        'correlationId': correlationId,
        'userId': userId,
        'type': type,
        'guid': guid,
        'email': email,
        'productId': productId,
        'updateOrderUrl': updateOrderUrl,
        'checkoutCompleteUrl': checkoutCompleteUrl,
        'appOrderId': appOrderId,
        'pssAppId': pssAppId,
        'grossTotalPrice':grossTotalPrice,
        'netTotalPrice':netTotalPrice,
        'vatTotalAmount':vatTotalAmount,
        'totalQuantity':totalQuantity,
        // TODO : Need to add a loop in case there are several line items
        // (never the case for a Pay As You Go Order / PAYG).
        'lineItems':[
          {
            'appLineItemId':lineItems[0]['appLineItemId'],
            'grossUnitPrice':lineItems[0]['grossUnitPrice'],
            'netUnitPrice':lineItems[0]['netUnitPrice'],
            'vatUnitAmount':lineItems[0]['vatUnitAmount'],
            'quantity':lineItems[0]['quantity']
            // TODO : Add a lineItems data array here.
          }
        ],
        'billingAddress':
        {
          'fullName':billingAddress['fullName'],
          'address1':billingAddress['address1'],
          'address2':billingAddress['address2'],
          'postTown':billingAddress['postTown'],
          'county':billingAddress['county'],
          'postcode':billingAddress['postcode'],
          'country':billingAddress['country']
        }
      };

      return order;
    }

    function createOrder(
      correlationId,
      userId,
      type,
      guid,
      email,
      productId,
      updateOrderUrl,
      checkoutCompleteUrl,
      appOrderId,
      pssAppId,
      grossTotalPrice,
      netTotalPrice,
      vatTotalAmount,
      totalQuantity,
      lineItems,
      billingAddress) {
      // Don't even call the http request an create the promise if the
      // number of arguments provided is not enough.
      // TODO : Return a proper exception ?
      if (arguments.length !== 16) {
        return false;
      }

      order = prepareCreateOrder(correlationId,
        userId,
        type,
        guid,
        email,
        productId,
        updateOrderUrl,
        checkoutCompleteUrl,
        appOrderId,
        pssAppId,
        grossTotalPrice,
        netTotalPrice,
        vatTotalAmount,
        totalQuantity,
        lineItems,
        billingAddress);

      var orderPromise = $http({
        method: 'POST',
        url: drupal.settings.rpss_drupal_order_service_base_url,
        headers: {
          'Content-Type': 'application/json'
        },
        data: order
      }).then(
        function successCallback (response) {
          return response.data;
        },
        function errorCallback (response) {
          return response.data;
        }
      );

      return orderPromise;
    }

    function settleOrder(orderId, data) {
      var
        orderPromise,
        settle_order_url = drupal.settings.rpss_drupal_order_service_base_url+'/'+orderId+'/settle';

      orderPromise = $http({
        method: 'POST',
        url: settle_order_url,
        headers: {
          'Content-Type': 'application/json'
        },
        data: data
      }).then(function successCallback(response) {
        return response.data;
      }, function errorCallback(response) {
        return response.data;
      });
      return orderPromise;
    }

    function getRandomCorrelationId() {
      return String(Math.floor(Math.random()*10000));
    }
  }

})();
