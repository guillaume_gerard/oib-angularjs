(function () {
  'use strict';

  /**
  * @ngdoc overview
  * @name oipApp
  * @description
  *   Base module for the oipApp application.
  */
  angular.module('oipApp', ['ngRoute']);

}());
