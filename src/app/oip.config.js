(function () {
  'use strict';

  angular.module('oipApp')
  .config(function($routeProvider) {
    $routeProvider
    // TODO Use relative paths to templates.
    .when('/', {
      templateUrl : 'src/app/paymentForm/paymentForm.view.html'
    })
    .when('/process-order/:orderId', {
      templateUrl : 'src/app/processOrder/processOrder.view.html'
    })
    .when('/thank-you/:orderId', {
      templateUrl : 'src/app/processOrder/thankYou.view.html'
    })
    .when('/sorry/:orderId', {
      templateUrl : 'src/app/processOrder/sorry.view.html'
    })
    .otherwise({
      redirectTo: '/'
    });
  });

}());
