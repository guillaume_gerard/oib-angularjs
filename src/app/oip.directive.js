(function () {
  'use strict';


  angular
    .module('oipApp')
    .directive('oipAlphanumeric', function () {
      // TODO : Put this constant in a Utility file.
      // See : https://docs.angularjs.org/api/auto/service/$provide#value
      var ALPHANUMERIC_REGEXP = /^[a-z0-9]+$/i;
      return {
        restrict:'AE',
        require:'ngModel',
        link: function(scope, element, attrs, ctrl) {
          ctrl.$validators.oipAlphanumeric = function(modelValue, viewValue) {
            if (ctrl.$isEmpty(modelValue)) {
              // Consider empty models to be valid.
              return true;
            }
            if (ALPHANUMERIC_REGEXP.test(viewValue)) {
              return true;
            }
            return false;
          };
        }
      };
    });

  angular
    .module('oipApp')
    .directive('oipNumberMaxTwoDigits', function () {
      // TODO : Put this constant in a Utility file.
      // See : https://docs.angularjs.org/api/auto/service/$provide#value
      var NUMBERMAXTWODIGITS_REGEXP = /^[0-9]+(\.[0-9]{1,2})?$/;
      return {
        restrict:'AE',
        require:'ngModel',
        link: function(scope, element, attrs, ctrl) {
          ctrl.$validators.oipAlphanumeric = function(modelValue, viewValue) {
            if (ctrl.$isEmpty(modelValue)) {
              // Consider empty models to be valid.
              return true;
            }
            if (NUMBERMAXTWODIGITS_REGEXP.test(viewValue)) {
              return true;
            }
            return false;
          };
        }
      };
    });

}());
