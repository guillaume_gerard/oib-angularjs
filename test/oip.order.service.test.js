describe('oipOrderService', function () {

  beforeEach(module('oipApp'));

  var oipOrder;

  beforeEach(inject(function(_oipOrder_){
    oipOrder = _oipOrder_;
  }));

  describe('Default localStorageOrder', function () {
    it('should be called oipOrders by default', function () {
      var localStorageName;
      localStorageName = oipOrder.getLocalStorageOrderName();
      expect(localStorageName).toBe('oipOrders');
    });
  });

  describe('Custom localStorageOrder', function () {

    beforeEach(inject(function(_oipOrder_){
      // Use the custom Local Storage Item before each test.
      oipOrder.setLocalStorageOrderName('testLocalStorage');
    }));

    it('should be able to create a new Local Storage Item and get the name', function () {
      var localStorageName;
      localStorageName = oipOrder.getLocalStorageOrderName();
      expect(localStorageName).toBe('testLocalStorage');
    });

    it('should be able to remove all Orders before starting our test', function () {
      if (oipOrder.getNumberOfOrder() !== 0) {
        // Empty the test local Storage Item.
        oipOrder.removeLocalStorageOrderName('testLocalStorage');
      }
    });

    it('should be able to add two new Order Items', function () {
      var
      firstOrderId,
      secondOrderId;
      firstOrderId = oipOrder.createOrderId();
      expect(firstOrderId).toBe('1');
      secondOrderId = oipOrder.createOrderId();
      expect(secondOrderId).toBe('2');
    });

    it('should be able to display the current number of Order Items', function () {
      var numberOfOrder;
      numberOfOrder = oipOrder.getNumberOfOrder();
      expect(numberOfOrder).toBe(2);
    });

    it('should be able to create a new Order Item and Update it with a remote Order Item value', function () {
      var
        expectedRemoteOrderId = 'REMOTE63',
        orderIdCreated,
        updateOrderResult,
        remoteOrderId;
        orderIdCreated = oipOrder.createOrderId();
      expect(orderIdCreated).toBe('3');
      updateOrderResult = oipOrder.updateOrderId(orderIdCreated, expectedRemoteOrderId);
      expect(updateOrderResult).toBe(true);
      remoteOrderId = oipOrder.getRemoteOrderId(orderIdCreated);
      expect(remoteOrderId).toBe(expectedRemoteOrderId);
    });

  });

});
