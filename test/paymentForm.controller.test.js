describe('PaymentFormController', function () {

  beforeEach(module('oipApp'));

  var $controller;

  beforeEach(inject(function(_$controller_){
    $controller = _$controller_;
  }));

  describe('updateGrossAmount', function () {
    it('should update the grossAmount with expected values', function () {
      var paymentFormController = $controller('PaymentFormController');

      // 10 + 20 = 30.
      paymentFormController.netAmount = 10;
      paymentFormController.vatAmount = 20;
      paymentFormController.updateGrossAmount();
      expect(paymentFormController.grossAmount).toBe(30);

      // 15.5 + 25.5 = 41.
      paymentFormController.netAmount = 15.5;
      paymentFormController.vatAmount = 25.5;
      paymentFormController.updateGrossAmount();
      expect(paymentFormController.grossAmount).toBe(41);

      // 15.96 + 25.5 = 41.46.
      paymentFormController.netAmount = 15.96;
      paymentFormController.vatAmount = 25.5;
      paymentFormController.updateGrossAmount();
      expect(paymentFormController.grossAmount).toBe(41.46);

      // 26.23 + 0.77 = 27.
      paymentFormController.netAmount = 26.23;
      paymentFormController.vatAmount = 0.77;
      paymentFormController.updateGrossAmount();
      expect(paymentFormController.grossAmount).toBe(27);

      // 6333.66 + 1499.33 = 7832.991.
      paymentFormController.netAmount = 6333.66;
      paymentFormController.vatAmount = 1499.33;
      paymentFormController.updateGrossAmount();
      expect(paymentFormController.grossAmount).toBe(7832.99);
    });

  });
});
